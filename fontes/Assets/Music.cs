﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {
	public  AudioClip[] clips;
	private  AudioSource audioSource;
	public bool mirou = false;
	public int nrcubo;

	// Use this for initialization
	void Start () {
		audioSource = FindObjectOfType<AudioSource>();
		audioSource.loop = false;
	}

	//Quando mira no botão, ele atualiza o bool mirou para poder exercutar
	//o comando update() e então fazer o audio aleatório tocar.
	public void atualizaBool(){
		mirou = true;
	}

	
	//Pega a musica randomicamente do clip
	private AudioClip GetRandomClip(){
		nrcubo = Random.Range(0,clips.Length);
		return clips[nrcubo];
	
	}

	
	public int getNrcubo(){
		return nrcubo;
	}
	
	// Update is called once per frame
	void Update () {
		if(mirou == true){
			if(!audioSource.isPlaying){
				audioSource.clip = GetRandomClip();
				audioSource.Play();
				
				mirou = false;
			}
		}
		
	}
}
